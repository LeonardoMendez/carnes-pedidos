import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainViewComponent } from './views/main-view/main-view.component';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainToolbarComponent } from './components/main-toolbar/main-toolbar.component';
import { CategoryCardComponent } from './components/category-card/category-card.component';
import { ItemCardComponent } from './components/item-card/item-card.component';
import { CategoryListViewComponent } from './views/category-list-view/category-list-view.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { CartViewComponent } from './views/cart-view/cart-view.component';
import { MessageFormViewComponent } from './views/message-form-view/message-form-view.component';
import { StaticFooterComponent } from './components/static-footer/static-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    MainToolbarComponent,
    CategoryCardComponent,
    ItemCardComponent,
    CategoryListViewComponent,
    FooterComponent,
    CartViewComponent,
    MessageFormViewComponent,
    StaticFooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatBadgeModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
