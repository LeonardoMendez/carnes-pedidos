import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ItemCard} from './model';
import {CommunicationService} from '../../services/communication.service';
import {ShopCart} from '../../models/shopCart';
import {ItemProductCantBuy} from '../../models/itemProduct';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.sass']
})
export class ItemCardComponent implements OnInit, OnChanges {

  @Input() itemCard: ItemCard;
  private shopCart: ShopCart;

  constructor(private sharedService: CommunicationService) {
  }

  async ngOnInit() {
    this.sharedService.subscribeShopCartObserver().subscribe( (cart: ShopCart) => {
      this.shopCart = cart;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.itemCard = changes.itemCard.currentValue;
  }


  addToCart() {
    let itemProductCart = new ItemProductCantBuy();
    itemProductCart = Object.assign(itemProductCart, this.itemCard.item) ;
    itemProductCart.cantBuy = 1;
    this.shopCart.items.push(itemProductCart);
    this.sharedService.shopCartChangueState(this.shopCart);
  }


  addMore() {
    let finned: ItemProductCantBuy;
    finned = this.shopCart.items.find((value: ItemProductCantBuy) => value.id === this.itemCard.item.id);
    finned.cant = finned.cant + 1;
    this.sharedService.shopCartChangueState(this.shopCart);
  }

  remove() {
    let finned: ItemProductCantBuy;
    finned = this.shopCart.items.find((value: ItemProductCantBuy) => value.id === this.itemCard.item.id);
    if (finned.cant > 1) {
      finned.cant = finned.cant - 1;
      this.sharedService.shopCartChangueState(this.shopCart);
    }
  }


  removeToCar() {
    let finned: ItemProductCantBuy;
    finned = this.shopCart.items.find((value: ItemProductCantBuy) => value.id === this.itemCard.item.id);
    const index = this.shopCart.items.indexOf(finned, 0);
    if (index > -1) {
      this.shopCart.items.splice(index, 1);
      this.itemCard.aggregated = false;
      this.sharedService.shopCartChangueState(this.shopCart);
    }
  }
}
