import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-static-footer',
  templateUrl: './static-footer.component.html',
  styleUrls: ['./static-footer.component.sass']
})
export class StaticFooterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToMessageForm() {
    this.router.navigate(['/message-form-view']);

  }
}
