import { Component, OnInit } from '@angular/core';
import {CommunicationService} from '../../services/communication.service';
import {ShopCart} from '../../models/shopCart';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.sass']
})
export class MainToolbarComponent implements OnInit {
  private shopCart: ShopCart;

  constructor(private communicationService: CommunicationService) {
    this.communicationService.subscribeShopCartObserver().subscribe((shopCart: ShopCart) => {
      this.shopCart = shopCart;
    });
  }

  ngOnInit() {
  }

}
