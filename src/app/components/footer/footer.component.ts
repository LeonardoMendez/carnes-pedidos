import { Component, OnInit } from '@angular/core';
import {ShopCart} from '../../models/shopCart';
import {CommunicationService} from '../../services/communication.service';
import {Router} from '@angular/router';
import {ItemProductCantBuy} from '../../models/itemProduct';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  private shopCart: ShopCart;
  private totalCount: number = 0;
  private totalItems: number = 0;

  constructor(private communicationService: CommunicationService, private router: Router) {  }

  ngOnInit() {
    this.communicationService.subscribeShopCartObserver().subscribe((shopCart: ShopCart) => {
      this.shopCart = shopCart;
      this.totalCount = 0;
      this.sumTotalPrice();
    });
  }

  goToCart() {
    this.router.navigate(['/cart-view']);
  }

  private sumTotalPrice() {
    this.shopCart.items.forEach( (itemProductCant: ItemProductCantBuy) => {
      this.totalCount =  this.totalCount + itemProductCant.cant * itemProductCant.amount;
    });
  }


}
