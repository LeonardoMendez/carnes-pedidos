export class ItemProduct {
  id: string;
  title: string;
  img: string;
  description: string;
  cant: number;
  typeCant: string;
  price: number;
  amount: number;
  stock: number;


  constructor() {

  }
}


export class ItemProductCantBuy extends ItemProduct {
  cantBuy;
  constructor() {
    super();
    this.cantBuy = 0;
  }
}

export class ItemProductCartView extends ItemProductCantBuy {
  arrowDown = true;
  constructor() {
    super();
  }
}
