import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ShopCart} from '../models/shopCart';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private observerShopCart: BehaviorSubject<ShopCart> = new BehaviorSubject<ShopCart>(new ShopCart());
  observerShopCart$: Observable<ShopCart> = this.observerShopCart.asObservable();

  constructor() { }

  shopCartChangueState(value: ShopCart) {
    this.observerShopCart.next(value);
  }

  subscribeShopCartObserver(): Observable<ShopCart>  {
    return this.observerShopCart$;
  }
}
