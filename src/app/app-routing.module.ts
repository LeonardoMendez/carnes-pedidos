import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainViewComponent } from './views/main-view/main-view.component';
import { CategoryListViewComponent } from './views/category-list-view/category-list-view.component';
import {CartViewComponent} from './views/cart-view/cart-view.component';
import {MessageFormViewComponent} from './views/message-form-view/message-form-view.component';


const routes: Routes = [
  { path: '',   redirectTo: '/main-view', pathMatch: 'full' },
  { path: 'main-view', component: MainViewComponent },
  { path: 'category-list/:category', component: CategoryListViewComponent },
  { path: 'cart-view', component: CartViewComponent },
  { path: 'message-form-view', component: MessageFormViewComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
