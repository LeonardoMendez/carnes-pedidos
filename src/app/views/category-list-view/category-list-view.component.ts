import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ItemProduct, ItemProductCantBuy} from '../../models/itemProduct';
import * as itemsProduct from '../../../assets/data-test/items-card.json';
import {CommunicationService} from '../../services/communication.service';
import {ShopCart} from '../../models/shopCart';
import {ItemCard} from '../../components/item-card/model';

@Component({
  selector: 'app-category-list-view',
  templateUrl: './category-list-view.component.html',
  styleUrls: ['./category-list-view.component.sass']
})
export class CategoryListViewComponent implements OnInit {

  private itemsProduct: ItemProduct[] = [];
  private itemsCards: ItemCard[] = [];

  constructor(private route: ActivatedRoute, private communicationService: CommunicationService) { }

  ngOnInit() {

    // TODO cargo los itemsProduct que hay
    this.itemsProduct = (itemsProduct as any).default;
    console.log(this.itemsProduct);

    this.route.paramMap.subscribe(params => {
      console.log('category for param:', params.get('category'));
    });

    // creo todas las cards
    this.itemsProduct.forEach( (itemProduct: ItemProduct) => {
      const itemAux = new ItemCard();
      itemAux.item = itemProduct;
      this.itemsCards.push(itemAux);
    });

    this.communicationService.subscribeShopCartObserver().subscribe( ( (shopCart: ShopCart) => {
      shopCart.items.forEach((itemProduct: ItemProductCantBuy) => {
        const itemCard = this.itemsCards.find((aItemCard: ItemCard) => aItemCard.item.id === itemProduct.id);
        itemCard.aggregated = true;
        itemCard.cant = itemProduct.cant;
      });
    }));


  }

}
