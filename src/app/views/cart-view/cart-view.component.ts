import {Component, Input, OnInit} from '@angular/core';
import {CommunicationService} from '../../services/communication.service';
import {ShopCart} from '../../models/shopCart';
import {ItemProductCantBuy, ItemProductCartView} from '../../models/itemProduct';
import {ItemCard} from '../../components/item-card/model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.sass']
})
export class CartViewComponent implements OnInit {
  private itemsCartView: ItemProductCartView[] = [];
  private mobile: boolean;

  constructor(private communicationService: CommunicationService) { }

  ngOnInit() {
    if (window.screen.width === 360) { // 768px portrait
      this.mobile = true;
    }
    this.communicationService.subscribeShopCartObserver().subscribe( ( (shopCart: ShopCart) => {

      shopCart.items.forEach( (item: ItemProductCantBuy) => {
        let itemAux = new ItemProductCartView();
        itemAux = Object.assign(itemAux, item);
        this.itemsCartView.push(itemAux);
      } );


    }));

  }



  delete() {

  }

  remove() {

  }

  addMore() {

  }


  toggleArrow(card: ItemProductCartView) {
    card.arrowDown = !card.arrowDown;
  }


}
