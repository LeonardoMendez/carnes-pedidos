import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageFormViewComponent } from './message-form-view.component';

describe('MessageFormViewComponent', () => {
  let component: MessageFormViewComponent;
  let fixture: ComponentFixture<MessageFormViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageFormViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFormViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
