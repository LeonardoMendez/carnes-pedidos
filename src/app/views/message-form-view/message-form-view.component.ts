import { Component, OnInit } from '@angular/core';
import {CostumerOrder} from './model';
import {ShopCart} from '../../models/shopCart';
import {ItemProductCantBuy, ItemProductCartView} from '../../models/itemProduct';
import {CommunicationService} from '../../services/communication.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-message-form-view',
  templateUrl: './message-form-view.component.html',
  styleUrls: ['./message-form-view.component.sass']
})
export class MessageFormViewComponent implements OnInit {

  model: CostumerOrder;
  private shopCart: ShopCart;

  constructor(private communicationService: CommunicationService) {
    this.model = new CostumerOrder();
  }

  ngOnInit() {
    this.communicationService.subscribeShopCartObserver().subscribe( ( (shopCart: ShopCart) => {
        this.shopCart = shopCart;
    }));
  }

  onFormSubmit(model: CostumerOrder) {
    console.log(model);
    const whatsappUrl = 'https://api.whatsapp.com/send?phone='+ environment.telephoneToSend + '&text=' + this.getOrderMessage(model);
    window.open(whatsappUrl, '_black');
  }

  private getOrderMessage(model: CostumerOrder) {

    const messageIntroduction = 'Hola soy ' + model.costumerName;
    const messageTelephone = ' mi telefono es: ' + model.costumerTelephone;
    const messageAddress = model.costumerAdress ? ' mi dirección es: ' + model.costumerComment : '';
    const messageOrder = 'Quiero encargarle ';
    let order = '';
    this.shopCart.items.forEach( (item: ItemProductCantBuy) => {
      order = order + 'el producto ' + item.title +
        'la cantidad ' + item.cant + ' ' + item.typeCant +
        'que tiene un costo de' + item.amount + '     ';
    } );
    return messageIntroduction + messageTelephone + messageAddress + messageOrder + order;
  }

}
